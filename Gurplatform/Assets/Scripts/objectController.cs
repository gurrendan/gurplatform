﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class objectController : MonoBehaviour
{
    public bool isUsable = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    public virtual void UseObject(unitController user) {
        Debug.Log(this.name + " used by " + user.name + "!");
    }
}
