﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class doorController : objectController
{
    public doorController targetDoor;
    public int doorID;
    // Start is called before the first frame update
    void Start()
    {
        this.isUsable = true;
    }

    // Update is called once per frame
    void Update()
    {

    }

    // перемещение между дверьми
    public override void UseObject(unitController user)
    {
        float userRelativePosX = this.transform.position.x - user.transform.position.x;
        float userRelativePosY = this.transform.position.y - user.transform.position.y;
        Debug.Log("userRelativePosX = " + userRelativePosX + " userRelativePosY = " + userRelativePosY);
        user.transform.position = new Vector2 (this.targetDoor.transform.position.x - userRelativePosX, this.targetDoor.transform.position.y - userRelativePosY);
    }
}
