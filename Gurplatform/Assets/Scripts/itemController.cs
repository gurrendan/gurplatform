﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class itemController : objectController
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    // перемещение между дверьми
    public override void UseObject(unitController user)
    {
        user.inventory.Add(this.GetComponent<itemController>());
        this.gameObject.GetComponent<SpriteRenderer>().enabled = false;
    }
}
