﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerController : unitController
{
    [HideInInspector] public bool facingRight = true;
    [HideInInspector] public bool jump = false;
    public float moveForce = 365f;
    public float maxSpeed = 5f;
    public float jumpForce = 1000f;
    public Transform groundCheck;


    private bool grounded = false;
    private Animator anim;
    private Rigidbody2D rb2d;
    private float inputSpeed = 0.1f;

    private objectController usingObject;

    // Use this for initialization
    void Awake()
    {
        anim = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        grounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Default"));

        if (Input.GetKeyDown(KeyCode.W) && grounded)
        {
            jump = true;
        }
    }

    void FixedUpdate()
    {
        //float h = Input.GetAxis("Horizontal");
        float h = Input.GetKey(KeyCode.A) ? -inputSpeed : (Input.GetKey(KeyCode.D) ? inputSpeed : 0);
        
        anim.SetFloat("Speed", Mathf.Abs(h));

        rb2d.MovePosition(new Vector2(this.transform.position.x + h, this.transform.position.y));

        /*if (h * rb2d.velocity.x < maxSpeed)
            rb2d.AddForce(Vector2.right * h * moveForce);

        if (Mathf.Abs(rb2d.velocity.x) > maxSpeed)
            rb2d.velocity = new Vector2(Mathf.Sign(rb2d.velocity.x) * maxSpeed, rb2d.velocity.y);*/

        if (h > 0 && !facingRight)
            Flip();
        else if (h < 0 && facingRight)
            Flip();

        if (jump)
        {
            //anim.SetTrigger("Jump");
            rb2d.AddForce(new Vector2(0f, jumpForce));
            jump = false;
        }

        if (Input.GetKeyDown(KeyCode.E)) {
            if (this.usingObject != null) {
                this.usingObject.UseObject(this);
            }
        }

        if (Input.GetKeyDown(KeyCode.I))
        {
            string itemsnames = "";
            foreach (itemController item in inventory)
            {
                itemsnames += itemsnames.Length==0 ? item.name : ", "+item.name;
            }
            Debug.Log("Inventory: " + itemsnames);
        }

    }

    void Flip()
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        Debug.Log("OnTriggerEnter2D");
                
        if (col.gameObject.GetComponent<objectController>() != null && col.gameObject.GetComponent<objectController>().isUsable) {
            this.usingObject = col.gameObject.GetComponent<objectController>();
            Debug.Log("using " + this.usingObject.name);
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        Debug.Log("OnTriggerExit2D");

        if (this.usingObject !=null && this.usingObject == col.gameObject.GetComponent<objectController>())
        {
            Debug.Log("leaves " + this.usingObject.name);
            this.usingObject = null;
        }
    }

}
